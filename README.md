The Jewelry Order Management System is implemented over the traditional Manual recording system, 
where the records were maintained in excel sheets and it becomes cumbersome to manage the transactions. 
This new system gives an easy way to access and manage the data for the employees, the implementation is as follows: 
• Enter the Jewelry Order Management System  
• Record the Customer details 
• Record information about the order placed by the customers.
• Record the Payment details
• Record and Manage the Product details 
• Generate Bills for Customer Orders. 


Software Requirements for the present project: 
 
WEB SERVER  : Apache WebServer 
LANGUAGES USED   : PHP 
DATABASE     : MySQL  
APPLICATION   : Web Application 